# -*- coding: UTF-8 -*-
__all__ = ['SummaryWriter', 'EventFileWriter']

from tsvis.logger.summary_writer import SummaryWriter
from tsvis.logger.writer import EventFileWriter

__version__ = 'dev'
__git_version__ = ''
